/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { ofType } from '@ngrx/effects';
import { ActionsSubject, Store } from '@ngrx/store';
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import * as store from '@shared/store';
import * as versionActions from '@shared/store/actions/version/version.action';
import { environment } from 'environments/environment';
import { ReplaySubject } from 'rxjs';
import { map, take, takeUntil } from 'rxjs/operators';

@Injectable()
export class VersionSandbox extends BaseSandbox {
  public version$: ReplaySubject<any> = new ReplaySubject();

  constructor(protected appState$: Store<store.State>, private actionsSubject: ActionsSubject) {
    super(appState$);
  }

  public loadVersion(): void {
    this.appState$.dispatch(versionActions.version());
    this.actionsSubject
      .pipe(
        ofType(versionActions.versionSuccess),
        map((action: versionActions.IVersionSuccess) => action.payload),
        take(1),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((payload: any) => {
        payload = { ...payload, frontendVersion: environment.version };
        this.version$.next(payload);
      });
  }
}
