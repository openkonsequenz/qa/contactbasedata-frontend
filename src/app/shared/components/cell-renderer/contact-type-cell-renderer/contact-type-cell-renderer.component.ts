 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit } from '@angular/core';
import { Globals } from '@shared/constants/globals';

@Component({
  selector: 'app-contact-type-cell-renderer',
  templateUrl: './contact-type-cell-renderer.component.html',
  styleUrls: ['./contact-type-cell-renderer.component.scss']
})
export class ContactTypeCellRendererComponent {

  public contactType: string;

  agInit(params: any): void {

    switch(params.value) {
      case Globals.CONTACT_TYPE_ID.EXTERNAL_PERSON:
        this.contactType = Globals.CONTACT_TYPE.EXTERNAL_PERSON;
        break;
      case Globals.CONTACT_TYPE_ID.INTERNAL_PERSON:
        this.contactType = Globals.CONTACT_TYPE.INTERNAL_PERSON;
        break;
      case Globals.CONTACT_TYPE_ID.COMPANY:
        this.contactType = Globals.CONTACT_TYPE.COMPANY;
        break;
      case Globals.CONTACT_TYPE_ID.CONTACT_PERSON:
        this.contactType = Globals.CONTACT_TYPE.CONTACT_PERSON;
        break;
      default:
        this.contactType = Globals.CONTACT_TYPE.UNKNOWN;
        break;
    }
  }
}
