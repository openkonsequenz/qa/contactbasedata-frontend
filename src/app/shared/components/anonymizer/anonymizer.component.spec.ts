/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AnonymizerComponent } from '@shared/components/anonymizer/anonymizer.component';

describe('AnonymizerComponent', () => {
  let component: AnonymizerComponent;
  const sandbox: any = {
    anonymizeContact: () => {},
    endSubscriptions: () => {},
  };

  beforeEach(() => {
    component = new AnonymizerComponent(sandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should transfer anonymizeContact call to sandbox', () => {
    const spy: any = spyOn(sandbox, 'anonymizeContact');
    component.anonymizeContact();
    expect(spy).toHaveBeenCalled();
  });

  it('should reset component', () => {
    component.contactId = 'x';
    component.ngOnDestroy();
    expect(component.contactId).toBe(null);
  });
});
