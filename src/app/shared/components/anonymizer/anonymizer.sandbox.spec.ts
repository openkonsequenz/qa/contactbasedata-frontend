/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AnonymizerSandbox } from '@shared/components/anonymizer/anonymizer.sandbox';
import { Store, ActionsSubject } from '@ngrx/store';
import { State } from '@shared/store';
import { of } from 'rxjs';
import { Router } from '@angular/router';

describe('AnonymizerSandbox', () => {
  let service: AnonymizerSandbox;
  let appState: Store<State>;
  let actionSubject: ActionsSubject;
  let router: Router;
  let modalService: NgbModal;

  beforeEach(() => {
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionSubject = { pipe: () => of(true) } as any;
    router = { navigateByUrl() {} } as any;
    modalService = { open() {} } as any;
    spyOn(appState, 'dispatch').and.callFake(() => {});

    service = new AnonymizerSandbox(appState, actionSubject, router, modalService);
  });

  it('should create SalutationsSandbox service', () => {
    expect(service).toBeTruthy();
  });

  it('should open modal before anonymizing an contact', () => {
    spyOn(service['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    service.anonymizeContact('x');
    expect(modalService.open).toHaveBeenCalled();
  });
});
