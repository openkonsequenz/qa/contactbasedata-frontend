/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AutoResizeColumnsDirective } from '@shared/directives/agGrid/auto-resize-columns.directive';
import { ServerSideDirective } from '@shared/directives/agGrid/server-side.directive';
import { TranslateColumnDefinitionsDirective } from '@shared/directives/agGrid/translate-column-definitions.directive';
import { FormDisableDirective } from '@shared/directives/form-disable.directive';
import { VisibleByRightDirective } from '@shared/directives/visible-by-right';
@NgModule({
  imports: [CommonModule],
  declarations: [AutoResizeColumnsDirective, TranslateColumnDefinitionsDirective, ServerSideDirective, FormDisableDirective, VisibleByRightDirective],
  exports: [AutoResizeColumnsDirective, TranslateColumnDefinitionsDirective, ServerSideDirective, FormDisableDirective, VisibleByRightDirective],
})
export class DirectivesModule {}
