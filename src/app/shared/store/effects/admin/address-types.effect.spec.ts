/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { take } from 'rxjs/operators';
import { AddressTypesEffects } from '@shared/store/effects/admin/address-types.effect';
import { Subject, of, throwError } from 'rxjs';
import { AddressType } from '@shared/models';
import * as addressTypesActions from '@shared/store/actions/admin/address-types.action';
import { AddressTypesApiClient } from '@pages/admin/address-types/address-types-api-client';
import { Store } from '@ngrx/store';

describe('AddressTypes Effects', () => {
  let effects: AddressTypesEffects;
  let actions$: Subject<any>;
  let apiClient: AddressTypesApiClient;
  let store: Store<any>;
  let apiResponse: any = null;

  beforeEach(() => {
    apiClient = {
      getAddressTypes() {},
      getAddressTypeDetails() {},
      putAddressType() {},
      postAddressType() {},
      deleteAddressType() {},
    } as any;
    store = {
      dispatch() {},
    } as any;
    actions$ = new Subject();
    effects = new AddressTypesEffects(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadAddressTypesSuccess after getAddressTypes', done => {
    apiResponse = [new AddressType({ id: '1' })];
    spyOn(apiClient, 'getAddressTypes').and.returnValue(of(apiResponse));
    effects.getAddressTypes$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(addressTypesActions.loadAddressTypesSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(addressTypesActions.loadAddressTypes());
  });

  it('should equal loadAddressTypesFail after getAddressType Error', done => {
    spyOn(apiClient, 'getAddressTypes').and.returnValue(throwError('x'));
    effects.getAddressTypes$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(addressTypesActions.loadAddressTypesFail({ payload: 'x' }));
    });
    done();
    actions$.next(addressTypesActions.loadAddressTypes());
  });

  it('should equal loadAddressTypeSuccess after getAddressType', done => {
    apiResponse = new AddressType({ id: '1' });
    spyOn(apiClient, 'getAddressTypeDetails').and.returnValue(of(apiResponse));
    effects.getAddressType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(addressTypesActions.loadAddressTypeSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(addressTypesActions.loadAddressType({ payload: '1' }));
  });

  it('should equal loadAddressTypeFail after getAddressType Error', done => {
    spyOn(apiClient, 'getAddressTypeDetails').and.returnValue(throwError('x'));
    effects.getAddressType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(addressTypesActions.loadAddressTypeFail({ payload: 'x' }));
    });
    done();
    actions$.next(addressTypesActions.loadAddressType({ payload: '1' }));
  });

  it('should equal saveAddressTypeSuccess after saveAddressType', done => {
    apiResponse = new AddressType({ id: '1' });
    spyOn(apiClient, 'putAddressType').and.returnValue(of(apiResponse));
    effects.saveAddressType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(addressTypesActions.saveAddressTypeSuccess({ payload: new AddressType({ id: '1' }) }));
    });
    done();
    actions$.next(addressTypesActions.saveAddressType({ payload: new AddressType({ id: '1' }) }));
  });

  it('should equal saveAddressTypeFail after saveAddressType Error', done => {
    spyOn(apiClient, 'putAddressType').and.returnValue(throwError('x'));
    effects.saveAddressType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(addressTypesActions.saveAddressTypeFail({ payload: 'x' }));
    });
    done();
    actions$.next(addressTypesActions.saveAddressType({ payload: new AddressType({ id: '1' }) }));
  });

  it('should equal deleteAddressTypeSuccess after deleteAddressType', done => {
    spyOn(apiClient, 'deleteAddressType').and.returnValue(of(null));
    effects.deleteAddressType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(addressTypesActions.deleteAddressTypeSuccess());
    });
    done();
    actions$.next(addressTypesActions.deleteAddressType({ payload: '1' }));
  });
});
