/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as keycloakUserActions from '@shared/store/actions/users/keycloak-user.action';
import { KeycloakUser } from '@shared/models';
import { Action, createReducer, on } from '@ngrx/store';

export interface State {
  loading: boolean;
  loaded: boolean;
  failed: boolean;
  data: Array<KeycloakUser>;
}

export const INITIAL_STATE: State = {
  loading: false,
  loaded: false,
  failed: false,
  data: [],
};
export const keycloakUserReducer = createReducer(
  INITIAL_STATE,
  on(keycloakUserActions.loadKeycloakUsers, (state: State | undefined, action: Action) => {
    return {
      ...state,
      loading: true,
      loaded: false,
      failed: false,
      data: [],
    };
  }),
  on(keycloakUserActions.loadKeycloakUsersSuccess, (state: State | undefined, action: Action) => {
    return {
      ...state,
      loading: false,
      loaded: true,
      failed: false,
      data: action['payload'],
    };
  }),
  on(keycloakUserActions.loadKeycloakUsersFail, (state: State | undefined, action: Action) => {
    return {
      ...state,
      loaded: false,
      loading: false,
      failed: true,
      data: [],
    };
  })
);
export function reducer(state = INITIAL_STATE, action: Action): State {
  return keycloakUserReducer(state, action);
}

export const getData = (state: State) => state.data;
export const getLoading = (state: State) => state.loading;
export const getLoaded = (state: State) => state.loaded;
export const getFailed = (state: State) => state.failed;
