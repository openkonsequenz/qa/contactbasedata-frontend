/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { CompanyAddressDetailsComponent } from '@pages/company/company-details/address-details/address-details.component';
import { of } from 'rxjs';

describe('AddressDetailsComponent', () => {
  let component: CompanyAddressDetailsComponent;
  let companySandbox: any;
  let addressTypesSandbox: any;
  let modalService: any;
  let actionsSubject: any;

  beforeEach(async(() => {
    companySandbox = {
      registerAddressEvents() {},
      registerCompanyEvents() {},
      endSubscriptions() {},
      clearAddressData() {},
      persistAddress() {},
      addressDetailsCurrentFormState: { value: {} },
      existMainAddress: null,
      isCurrentAddressMainAddress: null,
    } as any;

    modalService = {
      open() {},
    } as any;
  }));

  addressTypesSandbox = {};

  beforeEach(() => {
    component = new CompanyAddressDetailsComponent(companySandbox, addressTypesSandbox, modalService, actionsSubject);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call registerAddressEvents and clearAddress onInit', () => {
    const spy1 = spyOn(companySandbox, 'registerAddressEvents');
    const spy2 = spyOn(companySandbox, 'clearAddressData');
    component.ngOnInit();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });

  it('check if showDialog() works', () => {
    const spy1 = spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    companySandbox.addressDetailsCurrentFormState = { value: { isMainAddress: true } };
    companySandbox.existMainAddress = true;
    companySandbox.isCurrentAddressMainAddress = true;
    component.showDialog();
    expect(spy1).not.toHaveBeenCalled();

    companySandbox.isCurrentAddressMainAddress = false;
    component.showDialog();
    expect(spy1).toHaveBeenCalled();
  });

  it('should open modal before persist an address', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    component.companyDetailsSandbox.addressDetailsCurrentFormState.value.isMainAddress = true;
    component.companyDetailsSandbox.existMainAddress = true;
    component.companyDetailsSandbox.isCurrentAddressMainAddress = false;

    component.showDialog();
    expect(modalService.open).toHaveBeenCalled();
  });
});
