/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ContactPersonDetailsResolver } from '@pages/company/company-details/contact-person-details/contact-person-details.resolver';

describe('CompanyDetailsResolver', () => {
  let component: ContactPersonDetailsResolver;
  let contactPersonDetailsSandbox: any;
  let salutationsSandbox: any;
  let personTypesSandbox: any;
  let communicationTypesSandbox: any;
  let addressTypesSandbox: any;
  let userModuleAssignmentSandbox: any;

  beforeEach(() => {
    contactPersonDetailsSandbox = {
      companyContactId: null,
      loadContactPersonDetails() {},
      loadContactPersonAddresses() {},
      newContactPerson() {},
    } as any;

    communicationTypesSandbox = {
      loadCommunicationTypes() {},
    } as any;

    userModuleAssignmentSandbox = {
      loadFilteredUserModuleTypes() {},
      loadUserModuleAssignments() {},
    } as any;

    addressTypesSandbox = {
      loadAddressTypes() {},
    } as any;

    salutationsSandbox = {
      loadSalutations() {},
    } as any;

    personTypesSandbox = {
      loadPersonTypes() {},
    } as any;
  });

  beforeEach(() => {
    component = new ContactPersonDetailsResolver(
      contactPersonDetailsSandbox,
      salutationsSandbox,
      personTypesSandbox,
      communicationTypesSandbox,
      addressTypesSandbox,
      userModuleAssignmentSandbox
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ccc', () => {
    let ar: any = { params: { contactId: 'ID' } };
    component.resolve(ar);
    expect(contactPersonDetailsSandbox.companyContactId).toBe(ar.params.contactId);
  });

  it('should call clear if create a new contact-person', () => {
    const spy = spyOn(contactPersonDetailsSandbox, 'newContactPerson');
    let ar: any = { params: { contactPersonId: undefined } };
    component.resolve(ar);
    expect(spy).toHaveBeenCalled();
  });
});
