/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { ContactPersonCommunicationsDataDetailsComponent } from '@pages/company/company-details/contact-person-details/communications-data-details/communications-data-details.component';

describe('ContactPersonCommunicationsDataDetailsComponent', () => {
  let component: ContactPersonCommunicationsDataDetailsComponent;
  let sandbox: any;
  let communicationTypesSandbox: any;

  beforeEach(async(() => {
    sandbox = {
      registerCommunicationsDataEvents() {},
      registerCompanyEvents() {},
      endSubscriptions() {},
      clearCommunicationsData() {},
    } as any;

    communicationTypesSandbox = {} as any;
  }));

  beforeEach(() => {
    component = new ContactPersonCommunicationsDataDetailsComponent(sandbox, communicationTypesSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call registerCommunicationsDataEvents and clearCommunicationsData onInit', () => {
    const spy1 = spyOn(sandbox, 'registerCommunicationsDataEvents');
    const spy2 = spyOn(sandbox, 'clearCommunicationsData');
    component.ngOnInit();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });
});
