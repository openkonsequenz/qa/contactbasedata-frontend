import { Component, OnInit, OnDestroy } from '@angular/core';
import { PersonTypesSandbox } from '@pages/admin/person-types/person-types.sandbox';

@Component({
  selector: 'app-person-types-details',
  templateUrl: './person-types-details.component.html',
  styleUrls: ['./person-types-details.component.scss']
})
export class PersonTypesDetailsComponent implements OnInit, OnDestroy  {

  constructor(
    public personTypesSandbox: PersonTypesSandbox,
  ) { }

  ngOnInit() {
    this.personTypesSandbox.registerEvents();
  }

  ngOnDestroy() {
    this.personTypesSandbox.clear();
    this.personTypesSandbox.endSubscriptions();
  }

}
