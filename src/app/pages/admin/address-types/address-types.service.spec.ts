/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AddressType } from '@shared/models';
import { AddressTypesService } from '@pages/admin/address-types/address-types.service';

describe(' AddressTypesService', () => {
  let service:  AddressTypesService;

  beforeEach(() => {
  });

  it('should transform list', () => {
    const response = [new AddressType()];
    response[0].type = 'X';

    expect( AddressTypesService.gridAdapter(response)[0].type).toBe('X');
  });


  it('should transform details', () => {
    const response: any = { type: 'X'};
    const item =  AddressTypesService.itemAdapter(response);

    expect(item.type).toBe(response.type);
  });
});
