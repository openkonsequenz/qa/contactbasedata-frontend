import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommunicationTypesSandbox } from '@pages/admin/communication-types/communication-types.sandbox';

@Component({
  selector: 'app-communication-types-details',
  templateUrl: './communication-types-details.component.html',
  styleUrls: ['./communication-types-details.component.scss'],
})
export class CommunicationTypesDetailsComponent implements OnInit, OnDestroy {

  constructor(public communicationTypesSandbox: CommunicationTypesSandbox) {}

  ngOnInit() {
    this.communicationTypesSandbox.registerEvents();
  }

  ngOnDestroy() {
    this.communicationTypesSandbox.clear();
    this.communicationTypesSandbox.endSubscriptions();
  }
}
