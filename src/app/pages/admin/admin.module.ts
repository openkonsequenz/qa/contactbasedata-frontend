/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { TranslateModule } from '@ngx-translate/core';
import { CommunicationTypesApiClient } from '@pages/admin/communication-types/communication-types-api-client';
import { CommunicationTypesDetailsComponent } from '@pages/admin/communication-types/communication-types-details/communication-types-details.component';
import { CommunicationTypesSandbox } from '@pages/admin/communication-types/communication-types.sandbox';
import { CommunicationTypesService } from '@pages/admin/communication-types/communication-types.service';
import { SalutationsApiClient } from '@pages/admin/salutations/salutations-api-client';
import { SalutationsDetailsComponent } from '@pages/admin/salutations/salutations-details/salutations-details.component';
import { SalutationsSandbox } from '@pages/admin/salutations/salutations.sandbox';
import { SalutationsService } from '@pages/admin/salutations/salutations.service';
import { ComponentsModule } from '@shared/components';
import { ContainersModule } from '@shared/containers';
import { DirectivesModule } from '@shared/directives';
import { CommunicationTypesEffects } from '@shared/store/effects/admin/communication-types.effect';
import { SalutationsEffects } from '@shared/store/effects/admin/salutations.effect';
import { NgrxFormsModule } from 'ngrx-forms';

import { AddressTypesSandbox } from '@pages/admin/address-types//address-types.sandbox';
import { AddressTypesApiClient } from '@pages/admin/address-types/address-types-api-client';
import { AddressTypesDetailsComponent } from '@pages/admin/address-types/address-types-details/address-types-details.component';
import { AddressTypesService } from '@pages/admin/address-types/address-types.service';
import { AdminRoutingModule } from '@pages/admin/admin-routing.module';
import { PersonTypesApiClient } from '@pages/admin/person-types/person-types-api-client';
import { PersonTypesDetailsComponent } from '@pages/admin/person-types/person-types-details/person-types-details.component';
import { PersonTypesSandbox } from '@pages/admin/person-types/person-types.sandbox';
import { PersonTypesService } from '@pages/admin/person-types/person-types.service';
import { AddressTypesEffects } from '@shared/store/effects/admin/address-types.effect';
import { PersonTypesEffects } from '@shared/store/effects/admin/person-types.effect';
import { AgGridModule } from 'ag-grid-angular';
import { AdminComponent } from './admin.component';
import { AdminResolver } from './admin.resolver';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    ContainersModule,
    TranslateModule,
    DirectivesModule,
    ReactiveFormsModule,
    NgrxFormsModule,
    RouterModule,
    FormsModule,
    EffectsModule.forFeature([SalutationsEffects, CommunicationTypesEffects, PersonTypesEffects, AddressTypesEffects]),
    AgGridModule,
    AdminRoutingModule,
  ],
  declarations: [SalutationsDetailsComponent, CommunicationTypesDetailsComponent, PersonTypesDetailsComponent, AddressTypesDetailsComponent, AdminComponent],
  providers: [
    SalutationsService,
    SalutationsApiClient,
    SalutationsSandbox,
    CommunicationTypesSandbox,
    CommunicationTypesService,
    CommunicationTypesApiClient,
    PersonTypesSandbox,
    PersonTypesService,
    PersonTypesApiClient,
    AddressTypesSandbox,
    AddressTypesService,
    AddressTypesApiClient,
    AdminResolver,
  ],
})
export class AdminModule {}
