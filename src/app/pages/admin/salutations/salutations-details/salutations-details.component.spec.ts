 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { SalutationsDetailsComponent } from '@pages/admin/salutations/salutations-details/salutations-details.component';
import { SalutationsSandbox } from '@pages/admin/salutations/salutations.sandbox';

describe('SalutationsDetailsComponent', () => {
  let component: SalutationsDetailsComponent;
  let sandbox: SalutationsSandbox;

beforeEach(() => {
  sandbox = {
    registerEvents() {},
    endSubscriptions() {},
    clear() {},
  } as any;
  component = new SalutationsDetailsComponent(sandbox);
});

it('should create', () => {
  expect(component).toBeTruthy();
});

it('should registerEvents OnInit', () => {
  let spy = spyOn(sandbox,'registerEvents');
  component.ngOnInit();
  expect(spy).toHaveBeenCalled();
});

it('should endSubscriptions OnDestroy', () => {
  let spy = spyOn(sandbox,'endSubscriptions');
  component.ngOnDestroy();
  expect(spy).toHaveBeenCalled();
});
});
