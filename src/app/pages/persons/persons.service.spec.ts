import { LdapUser } from '@shared/models/users/ldap-user.model';
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PersonsService } from '@pages/persons/persons.service';
import { Address, CommunicationsData, KeycloakUser } from '@shared/models';

describe('PersonsService', () => {
  beforeEach(() => {});

  it('should transform externalPerson details', () => {
    const externalPerson: any = { lastName: 'Heysterkamp' };
    const transContact = PersonsService.externalPersonDetailsAdapter(externalPerson);

    expect(transContact.lastName).toBe(externalPerson.lastName);
  });

  it('should transform internalPerson details', () => {
    const internalPerson: any = { lastName: 'Heysterkamp' };
    const transContact = PersonsService.internalPersonDetailsAdapter(internalPerson);

    expect(transContact.lastName).toBe(internalPerson.lastName);
  });

  it('should transform internalPersons', () => {
    const internalPersons: any = { content: [{ lastName: 'Heysterkamp' }] };
    const transContact = PersonsService.internalPersonsAdapter(internalPersons);

    expect(transContact[0].lastName).toBe(internalPersons.content[0].lastName);
  });

  it('should transform externalPerson addresss details', () => {
    const externalPersonAddress: any = { addressTypeType: 'Heysterkamp' };
    const transContact = PersonsService.addressesDetailsAdapter(externalPersonAddress);

    expect(transContact.addressTypeType).toBe(externalPersonAddress.addressTypeType);
  });

  it('should transform addressses list', () => {
    const response = [new Address()];
    response[0].addressTypeType = 'Herr';

    expect(PersonsService.addressesAdapter(response)[0].addressTypeType).toBe('Herr');
  });

  it('should transform communicationa data details', () => {
    const communicationsData: any = { communicationTypeId: 'Heysterkamp' };
    const transContact = PersonsService.communicationsDataDetailsAdapter(communicationsData);

    expect(transContact.communicationTypeId).toBe(communicationsData.communicationTypeId);
  });

  it('should transform communicationa data list', () => {
    const response = [new CommunicationsData()];
    response[0].communicationTypeId = 'Herr';

    expect(PersonsService.communicationsDataAdapter(response)[0].communicationTypeId).toBe('Herr');
  });

  it('should transform KeycloakUser data list', () => {
    const response = [new KeycloakUser()];
    response[0].username = 'Herr';

    expect(PersonsService.keycloakUsersAdapter(response)[0].username).toBe('Herr');
  });

  it('should transform LdapUser data list', () => {
    const response = [new LdapUser()];
    response[0].uid = 'Herr';

    expect(PersonsService.ldapUsersAdapter(response)[0].uid).toBe('Herr');
  });
});
