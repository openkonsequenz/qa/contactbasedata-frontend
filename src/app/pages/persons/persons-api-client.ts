/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpService, GET, Path, Adapter, PUT, Body, DefaultHeaders, POST, DELETE, Query } from '@shared/asyncServices/http';
import { Observable } from 'rxjs';
import { PersonsService } from '@pages/persons/persons.service';
import { ExternalPerson, InternalPerson, Address, CommunicationsData, KeycloakUser, LdapUser } from '@shared/models';
import { InternalPersonInterface } from '@shared/models/persons/internal-person-arguments.interface';

@Injectable()
@DefaultHeaders({
  Accept: 'application/json',
  'Content-Type': 'application/json',
})
export class PersonsApiClient extends HttpService {
  /**
   * Retrieves external contact details by a given contactId
   *
   * @param contactId
   */
  @GET('/external-persons/{contactId}')
  @Adapter(PersonsService.externalPersonDetailsAdapter)
  public getExternalPersonDetails(@Path('contactId') contactId: string): Observable<ExternalPerson> {
    return null;
  }

  /**
   * Retrieves contact details addresses by a given contactId
   *
   * @param contactId
   */
  @GET('/contacts/{contactId}/addresses')
  @Adapter(PersonsService.addressesAdapter)
  public getAddresses(@Path('contactId') contactId: string): Observable<Address[]> {
    return null;
  }

  /**
   * Retrieves contact details addresses details by a given contactId and addressId
   *
   * @param contactId
   */
  @GET('/contacts/{contactId}/addresses/{addressId}')
  @Adapter(PersonsService.addressesDetailsAdapter)
  public getAddressesDetails(@Path('contactId') contactId: string, @Path('addressId') addressId: string): Observable<Address> {
    return null;
  }

  /**
   * Change the external person details by a given contactId
   *
   * @param contactId
   * @param editedExternalPerson
   */
  @PUT('/external-persons/{contactId}')
  @Adapter(PersonsService.externalPersonDetailsAdapter)
  public putExternalPersonDetails(@Path('contactId') contactId: string, @Body() editedExternalPerson: ExternalPerson): Observable<ExternalPerson> {
    return null;
  }

  /**
   * Saves new external person details.
   *
   * @param newExternalPerson
   */
  @POST('/external-persons')
  @Adapter(PersonsService.externalPersonDetailsAdapter)
  public postExternalPersonDetails(@Body() newExternalPerson: ExternalPerson): Observable<ExternalPerson> {
    return null;
  }

  /**
   * Retrieves internal contact details by a given contactId
   *
   * @param contactId
   */
  @GET('/internal-persons/{contactId}')
  @Adapter(PersonsService.internalPersonDetailsAdapter)
  public getInternalPersonDetails(@Path('contactId') contactId: string): Observable<InternalPerson> {
    return null;
  }

  /**
   * Change the internal person details by a given contactId
   *
   * @param contactId
   * @param editedInternalPerson
   */
  @PUT('/internal-persons/{contactId}')
  @Adapter(PersonsService.internalPersonDetailsAdapter)
  public putInternalPersonDetails(@Path('contactId') contactId: string, @Body() editedInternalPerson: InternalPerson): Observable<InternalPerson> {
    return null;
  }

  /**
   * Saves new internal person details.
   *
   * @param newInternalPerson
   */
  @POST('/internal-persons')
  @Adapter(PersonsService.internalPersonDetailsAdapter)
  public postInternalPersonDetails(@Body() newInternalPerson: InternalPerson): Observable<InternalPerson> {
    return null;
  }

  /**
   * Get internal persons.
   *
   * @param uid
   * @param userRef
   */
  @GET('/internal-persons')
  @Adapter(PersonsService.internalPersonsAdapter)
  private _getInternalPersons(@Query('uid') uid?: string, @Query('userRef') userRef?: string): Observable<Array<InternalPerson>> {
    return null;
  }

  public getInternalPersons(request: InternalPersonInterface): Observable<Array<InternalPerson>> {
    if (request != null) {
      return this._getInternalPersons(request.uid, request.userRef);
    }
  }

  /**
   * Change the address details by a given addressId
   *
   * @param contactId
   * @param addressId
   * @param editedAddress
   */
  @PUT('/contacts/{contactId}/addresses/{addressId}')
  @Adapter(PersonsService.addressesDetailsAdapter)
  public putAddressDetails(@Path('contactId') contactId: string, @Path('addressId') addressId: string, @Body() editedAdress: Address): Observable<Address> {
    return null;
  }

  /**
   * Saves new address details.
   *
   * @param contactId
   */
  @POST('/contacts/{contactId}/addresses')
  @Adapter(PersonsService.addressesDetailsAdapter)
  public postAddressDetails(@Path('contactId') contactId: string, @Body() newExternalPersonAddress: Address): Observable<Address> {
    return null;
  }

  /**
   * Deletes by a given id
   *
   * @param contactId
   * @param addressId
   */
  @DELETE('/contacts/{contactId}/addresses/{addressId}')
  @Adapter(PersonsService.addressesDetailsAdapter)
  public deleteAddress(@Path('contactId') contactId: string, @Path('addressId') addressId: string): Observable<void> {
    return null;
  }

  /**
   * Retrieves communications data by a given contactId
   *
   * @param contactId
   */
  @GET('/contacts/{contactId}/communications')
  @Adapter(PersonsService.communicationsDataAdapter)
  public getCommunicationsData(@Path('contactId') contactId: string): Observable<CommunicationsData[]> {
    return null;
  }

  /**
   * Retrieves communications data details by a given contactId and communicationId
   *
   * @param contactId
   * @param communicationId
   */
  @GET('/contacts/{contactId}/communications/{communicationId}')
  @Adapter(PersonsService.communicationsDataDetailsAdapter)
  public getCommunicationsDataDetails(@Path('contactId') contactId: string, @Path('communicationId') communicationId: string): Observable<CommunicationsData> {
    return null;
  }

  /**
   * Change the communications data details by a given communicationId
   *
   * @param contactId
   * @param communicationId
   * @param editedCommunicationsData
   */
  @PUT('/contacts/{contactId}/communications/{communicationId}')
  @Adapter(PersonsService.communicationsDataDetailsAdapter)
  public putCommunicationsDataDetails(
    @Path('contactId') contactId: string,
    @Path('communicationId') communicationId: string,
    @Body() editedCommunicationsData: CommunicationsData
  ): Observable<CommunicationsData> {
    return null;
  }

  /**
   * Saves new address details.
   *
   * @param contactId
   * @param newcommunicationsData
   */
  @POST('/contacts/{contactId}/communications')
  @Adapter(PersonsService.communicationsDataDetailsAdapter)
  public postCommunicationsDataDetails(
    @Path('contactId') contactId: string,
    @Body() newcommunicationsData: CommunicationsData
  ): Observable<CommunicationsData> {
    return null;
  }

  /**
   * Deletes by a given id
   *
   * @param contactId
   * @param communicationId
   */
  @DELETE('/contacts/{contactId}/communications/{communicationId}')
  @Adapter(PersonsService.communicationsDataDetailsAdapter)
  public deleteCommunicationsData(@Path('contactId') contactId: string, @Path('communicationId') communicationId: string): Observable<void> {
    return null;
  }

  /**
   * Retrieves all keycloak users.
   *
   */
  @GET('/user/keycloak-users')
  @Adapter(PersonsService.keycloakUsersAdapter)
  public getKeycloakUsers(): Observable<KeycloakUser[]> {
    return null;
  }

  /**
   * Retrieves all ldap users.
   *
   */
  @GET('/ldap/users')
  @Adapter(PersonsService.ldapUsersAdapter)
  public getLdapUsers(): Observable<LdapUser[]> {
    return null;
  }
}
