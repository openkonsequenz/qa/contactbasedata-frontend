/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { InternalPersonResolver } from '@pages/persons/internal-person/internal-person.resolver';

describe('InternalPersonResolver', () => {
  let component: InternalPersonResolver;
  let internalPersonSandbox: any;
  let userModuleAssignmentSandbox: any;
  let salutationsSandbox: any;
  let communicationTypesSandbox: any;
  let personTypesSandbox: any;
  let addressTypesSandbox: any;

  beforeEach(async(() => {
    internalPersonSandbox = {
      newInternalPerson() {},
      loadInternalPerson() {},
      loadCommunicationsData() {},
      loadInternalPersonAddresses() {},
      loadKeycloakUsers() {},
      loadLdapUsers() {},
    } as any;

    userModuleAssignmentSandbox = {
      loadFilteredUserModuleTypes() {},
      loadUserModuleAssignments() {},
    } as any;

    salutationsSandbox = {
      loadSalutations() {},
    } as any;

    personTypesSandbox = {
      loadPersonTypes() {},
    } as any;

    communicationTypesSandbox = {
      loadCommunicationTypes() {},
    } as any;

    addressTypesSandbox = {
      loadAddressTypes() {},
    } as any;
  }));

  beforeEach(() => {
    component = new InternalPersonResolver(
      internalPersonSandbox,
      userModuleAssignmentSandbox,
      salutationsSandbox,
      communicationTypesSandbox,
      personTypesSandbox,
      addressTypesSandbox
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call loadInternalPerson if edit a internal person', () => {
    const spy = spyOn(internalPersonSandbox, 'loadInternalPerson');
    let ar: any = { params: { contactId: 'ID' } };
    component.resolve(ar);
    expect(spy).toHaveBeenCalled();
  });

  it('should call clear if create a new internal person', () => {
    const spy = spyOn(internalPersonSandbox, 'newInternalPerson');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);
    expect(spy).toHaveBeenCalled();
  });

  it('should loadSalutations on resolve', () => {
    const spy = spyOn(salutationsSandbox, 'loadSalutations');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });

  it('should loadCommunicationTypes on resolve', () => {
    const spy = spyOn(communicationTypesSandbox, 'loadCommunicationTypes');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });

  it('should loadFilteredUserModuleTypes on resolve', () => {
    const spy = spyOn(userModuleAssignmentSandbox, 'loadFilteredUserModuleTypes');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });

  it('should set isCommunicationsDataDetailViewVisible true on resolve', () => {
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(internalPersonSandbox.isCommunicationsDataDetailViewVisible).toBeFalsy();
  });

  it('should set isDetailViewVisible true on resolve', () => {
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(internalPersonSandbox.isDetailViewVisible).toBeFalsy();
  });

  it('should loadPersonTypes on resolve', () => {
    const spy = spyOn(personTypesSandbox, 'loadPersonTypes');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });

  it('should loadAddressTypes on resolve', () => {
    const spy = spyOn(addressTypesSandbox, 'loadAddressTypes');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });

  it('should loadKeycloakUsers on resolve', () => {
    const spy = spyOn(internalPersonSandbox, 'loadKeycloakUsers');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });

  it('should loadLdapUsers on resolve', () => {
    const spy = spyOn(internalPersonSandbox, 'loadLdapUsers');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });
});
