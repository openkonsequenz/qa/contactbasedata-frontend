/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { InternalPersonCommunicationsDataDetailsComponent } from '@pages/persons/internal-person/internal-person-details/communications-data-details/communications-data-details.component';

describe('InternalPersonCommunicationsDataDetailsComponent', () => {
  let component: InternalPersonCommunicationsDataDetailsComponent;
  let internalPersonSandbox: any;
  let communicationTypesSandbox: any;

  beforeEach(() => {
    internalPersonSandbox = {
      registerCommunicationsDataEvents() {},
      registerInternalPersonEvents() {},
      endSubscriptions() {},
      clearCommunicationsData() {},
    } as any;

    communicationTypesSandbox = {
    } as any;
  });

  beforeEach(() => {
    component = new InternalPersonCommunicationsDataDetailsComponent(internalPersonSandbox, communicationTypesSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call registerCommunicationsDataEvents and clearCommunicationsData onInit', () => {
    const spy1 = spyOn(internalPersonSandbox, 'registerCommunicationsDataEvents');
    const spy2 = spyOn(internalPersonSandbox, 'clearCommunicationsData');
    component.ngOnInit();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });
});
