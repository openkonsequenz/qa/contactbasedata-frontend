/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { ExternalPersonAddressDetailsComponent } from '@pages/persons/external-person/external-person-details/address-details/address-details.component';
import { ExternalPersonAddressListComponent } from '@pages/persons/external-person/external-person-details/address-list/address-list.component';
import { ExternalPersonCommunicationsDataDetailsComponent } from '@pages/persons/external-person/external-person-details/communications-data-details/communications-data-details.component';
import { ExternalPersonCommunicationsDataListComponent } from '@pages/persons/external-person/external-person-details/communications-data-list/communications-data-list.component';
import { ExternalPersonDetailsComponent } from '@pages/persons/external-person/external-person-details/external-person-details.component';
import { ExternalPersonDetailsSandBox } from '@pages/persons/external-person/external-person-details/external-person-details.sandbox';
import { ExternalPersonResolver } from '@pages/persons/external-person/external-person.resolver';
import { InternalPersonAddressDetailsComponent } from '@pages/persons/internal-person/internal-person-details/address-details/address-details.component';
import { InternalPersonAddressListComponent } from '@pages/persons/internal-person/internal-person-details/address-list/address-list.component';
import { InternalPersonCommunicationsDataDetailsComponent } from '@pages/persons/internal-person/internal-person-details/communications-data-details/communications-data-details.component';
import { InternalPersonCommunicationsDataListComponent } from '@pages/persons/internal-person/internal-person-details/communications-data-list/communications-data-list.component';
import { InternalPersonDetailsComponent } from '@pages/persons/internal-person/internal-person-details/internal-person-details.component';
import { InternalPersonDetailsSandBox } from '@pages/persons/internal-person/internal-person-details/internal-person-details.sandbox';
import { InternalPersonResolver } from '@pages/persons/internal-person/internal-person.resolver';
import { PersonsApiClient } from '@pages/persons/persons-api-client';
import { PersonsRoutingModule } from '@pages/persons/persons.routing.module';
import { PersonsService } from '@pages/persons/persons.service';
import { ComponentsModule } from '@shared/components';
import { ContainersModule } from '@shared/containers';
import { DirectivesModule } from '@shared/directives';
import { personsReducers } from '@shared/store';
import { ExternalPersonEffect } from '@shared/store/effects/persons/external-person.effect';
import { InternalPersonEffect } from '@shared/store/effects/persons/internal-person.effect';
import { KeycloakUserEffect } from '@shared/store/effects/users/keycloak-user.effect';
import { LdapUserEffect } from '@shared/store/effects/users/ldap-user.effect';
import { AgGridModule } from 'ag-grid-angular';
import { NgrxFormsModule } from 'ngrx-forms';
import { AddressTypesSandbox } from '../admin/address-types/address-types.sandbox';
import { CommunicationTypesSandbox } from '../admin/communication-types/communication-types.sandbox';
import { PersonTypesSandbox } from '../admin/person-types/person-types.sandbox';
import { SalutationsSandbox } from '../admin/salutations/salutations.sandbox';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    TranslateModule,
    DirectivesModule,
    ReactiveFormsModule,
    RouterModule,
    NgrxFormsModule,
    FormsModule,
    StoreModule.forFeature('personsData', personsReducers),
    AgGridModule,
    ContainersModule,
    PersonsRoutingModule,
    EffectsModule.forFeature([ExternalPersonEffect, InternalPersonEffect, KeycloakUserEffect, LdapUserEffect]),
    NgbModule,
    FontAwesomeModule,
  ],
  declarations: [
    ExternalPersonDetailsComponent,
    InternalPersonDetailsComponent,
    ExternalPersonCommunicationsDataListComponent,
    ExternalPersonCommunicationsDataDetailsComponent,
    InternalPersonCommunicationsDataListComponent,
    InternalPersonCommunicationsDataDetailsComponent,
    ExternalPersonAddressListComponent,
    InternalPersonAddressListComponent,
    ExternalPersonAddressDetailsComponent,
    InternalPersonAddressDetailsComponent,
  ],
  providers: [
    AddressTypesSandbox,
    PersonTypesSandbox,
    CommunicationTypesSandbox,
    PersonsService,
    PersonsApiClient,
    ExternalPersonDetailsSandBox,
    ExternalPersonResolver,
    InternalPersonDetailsSandBox,
    InternalPersonResolver,
    SalutationsSandbox,
  ],
})
export class PersonsModule {}
