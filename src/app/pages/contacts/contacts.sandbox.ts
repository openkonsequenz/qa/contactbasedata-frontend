/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { BaseListSandbox } from '@shared/sandbox/base-list.sandbox';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as store from '@shared/store';
import * as contactsActions from '@shared/store/actions/contacts.action';
import { ServerSideModel } from '@shared/models/server-side.model';
import { Observable } from 'rxjs';

@Injectable()
export class ContactsSandbox extends BaseListSandbox {
  public contactListLoading$: Observable<boolean> = this.appState$.select(store.getContactsPageLoading);

  /**
   * Creates an instance of ContactsSandbox.
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @param {Store<store.State>} appState$
   * @param {ActionsSubject} actionsSubject
   * @param {Router} router
   * @param {UtilService} utilService
   * @memberof ContactsSandbox
   */
  constructor(protected appState$: Store<store.State>) {
    super(appState$);
    this.serversideModel = {
      ...this.serversideModel,
      ...{
        loadAction: contactsActions.loadContactsPage,
        successAction: contactsActions.loadContactsPageSuccess,
        pageSize: this.pageSize,
      },
    };
  }
}
