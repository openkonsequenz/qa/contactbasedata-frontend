/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ContactsSandbox } from '@pages/contacts/contacts.sandbox';
import { Store, ActionsSubject } from '@ngrx/store';
import { State } from '@shared/store';
import { UtilService } from '@shared/utility/utility.service';
import { of } from 'rxjs';
import { Router } from '@angular/router';

describe('ContactsSandbox', () => {
  let service: ContactsSandbox = {} as any;
  let appState: Store<State>;

  beforeEach(() => {
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;

    service = new ContactsSandbox(appState);
  });

  it('should create ContactsSandbox service', () => {
    expect(service).toBeTruthy();
    expect(service).toBeDefined();
  });
});
