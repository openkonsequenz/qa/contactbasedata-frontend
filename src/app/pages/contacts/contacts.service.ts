 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PageModel } from '@shared/models/page/page.model';
import { Injectable } from '@angular/core';
import { Contact } from '@shared/models';

/**
 * Used to retrieve contacts
 *
 * @export
 * @class ContactsService
 */
@Injectable()
export class ContactsService {
  /**
   * Transforms grid data recieved from the API into array of PageModel instances
   *
   * @param contacts
   */
  static gridPageAdapter(response: PageModel<Contact>): PageModel<Contact> {
    return response;
  }
  /**
   * Transforms contact details recieved from the API into instance of 'Contact'
   *
   * @param contact
   */
  static contactDetailsAdapter(contact: any): Contact {
    return new Contact(contact);
  }
}
