/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import { Store, ActionsSubject } from '@ngrx/store';
import * as store from '@shared/store';
import * as logoutActions from '@shared/store/actions/logout/logout.action';
import { ofType } from '@ngrx/effects';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class LogoutPageSandbox extends BaseSandbox {
  constructor(protected appState$: Store<store.State>, protected actionsSubject: ActionsSubject, private router: Router) {
    super(appState$);
  }

  public logout(): void {
    this.appState$.dispatch(logoutActions.logout());
    this.actionsSubject.pipe(ofType(logoutActions.logoutSuccess), takeUntil(this._endSubscriptions$)).subscribe(() => {
      this.clearStorage();
      this.removeUser();
      this.router.navigateByUrl('/loggedout');
    });
  }

  public goToOverview(): void {
    this.router.navigateByUrl('/overview');
  }
}
