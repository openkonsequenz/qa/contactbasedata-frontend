/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { APP_BASE_HREF } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ConfigService {
  private config: Object;
  private env: Object;

  constructor(private httpClient: HttpClient, @Inject(APP_BASE_HREF) private baseHref: string) {}

  /**
   * Loads the environment config file first. Reads the environment variable from the file
   * and based on that loads the appropriate configuration file - development or production
   */
  load() {
    return new Promise((resolve, reject) => {
      const headers = new HttpHeaders({
        Accept: 'application/json',
        'Content-Type': 'application/json',
        DataType: 'application/json',
      });

      this.httpClient.get(this.baseHref + 'config/env.json').subscribe(env_data => {
        this.env = env_data;

        this.httpClient
          .get(this.baseHref + 'config/' + env_data['env'] + '.json')
          .pipe(
            catchError((error: any) => {
              return throwError(error.error || 'Server error');
            })
          )
          .subscribe(data => {
            this.config = data;
            resolve(true);
          });
      });
    });
  }

  /**
   * Returns environment variable based on given key
   *
   * @param key
   */
  getEnv(key: any) {
    return this.env[key];
  }

  /**
   * Returns configuration value based on given key
   *
   * @param key
   */
  get(key: any) {
    return this.config[key];
  }
}
