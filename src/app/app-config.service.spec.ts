 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { ConfigService } from '@app/app-config.service';

describe('ConfigService', () => {
  let component: ConfigService;
  let http: any;
  let baseHref: any;

  beforeEach(async(() => {
    http = {
    } as any;

  }));

  beforeEach(() => {
    component = new ConfigService(http, baseHref);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
